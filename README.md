# `Next Generation Frontend Build Tools`

## Bundling and dev server tools

- [ESM or 'native modules'](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Modules) 🤟

- [esbuild](https://esbuild.github.io/) ✨ 👍 ⚠️

- [vite](https://vitejs.dev/) ✨ 👍 👍 ⚠️

## Not demoed but worth exploring further

- [snowpack](https://www.snowpack.dev/) ✨ ⚠️ ⚠️

- [wmr](https://github.com/preactjs/wmr) ✨ 👍 ⚠️ ⚠️

## Dependency management and monorepo tools

- [pnpm](https://pnpm.io/) 🪨 👍 ⚠️

- [npm workspaces](https://docs.npmjs.com/cli/v7/using-npm/workspaces) 💤

- [rushjs](https://rushjs.io/) 🤔 ⚠️ ⚠️

- [yarn2](https://yarnpkg.com/getting-started/install) 🤦 ⚠️ ⚠️

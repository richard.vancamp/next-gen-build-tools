import './App.css'
import '@monad/kit/dist/style.css'

import { TimeBox } from '@monad/kit'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h2>Vite App</h2>
        <TimeBox />
      </header>
    </div>
  )
}

export default App
